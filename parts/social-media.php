<?php global $wpApp; ?>
<ul class="social-media">
	<li class="social"><a target="_blank" href="<?= $wpApp['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
	<li class="social"><a target="_blank" href="<?= $wpApp['youtube']; ?>"><i class="fa fa-youtube"></i></a></li>
	<li class="social"><a target="_blank" href="<?= $wpApp['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>	
	<li class="social"><a target="_blank" href="<?= $wpApp['google']; ?>"><i class="fa fa-gplus"></i></a></li>	
	<li class="social"><a target="_blank" href="<?= $wpApp['linkedin']; ?>"><i class="fa fa-linkedin"></i></a></li>	
</ul>
